Template.home.onRendered(function () {
    var map = L.map('map');

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGhjYW1wMjAxNmRpYWIiLCJhIjoiY2lseXpqbDdrMDBpOHVqbTRiZGJ6ZWlycCJ9.80aabrLchI7k1E4mSLy_Zg', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
        id: 'hhcamp2016diab.basic'
    }).addTo(map);
});