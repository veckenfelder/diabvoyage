if (Meteor.isClient) {
    Router.map(function () {
        this.route('home', {
            path: '/',
            layoutTemplate: 'main'
        });

        this.route('account', {
            path: '/account',
            layoutTemplate: 'main'
        });

        this.route('countryTemplate', {
            path: '/countryTemplate',
            layoutTemplate: 'main'
        });
    })
}
