Package.describe({
  name: 'dek:leaflet',
  version: '1.0.0',
  // Brief, one-line summary of the package.
  summary: 'Meteor package for leaflet 1.0 beta 2, the open-source JS library for mobile-friendly interactive maps',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.addFiles([
    'lib/leaflet.js',
    'lib/leaflet.css',
  ], 'client');
  api.addAssets([
    'lib/images/layers-2x.png',
    'lib/images/layers.png',
    'lib/images/marker-icon-2x.png',
    'lib/images/marker-icon.png',
    'lib/images/marker-shadow.png'
  ],'client');
});
